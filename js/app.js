const btnBuscar = document.getElementById('btnBuscar');

btnBuscar.addEventListener('click', function(){
  const userId = document.getElementById("userId").value;
  axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`)
    .then(response => {
      const user = response.data;
      document.getElementById("name").value = user.name;
      document.getElementById("username").value = user.username;
      document.getElementById("email").value = user.email;
      document.getElementById("calle").value = 'Calle: ' + user.address.street;  
      document.getElementById("numero").value = 'Numero: ' + user.address.suite;
      document.getElementById("ciudad").value = 'Ciudad: ' + user.address.city;
    })
    .catch(error => {
      console.log(error);
      alert("No se encontró un usuario con ese ID.");
    });
})

document.getElementById("btnLimpiar").addEventListener("click", function(){
    document.getElementById("name").value = "";
    document.getElementById("username").value = "";
    document.getElementById("email").value = "";
    document.getElementById("calle").value = "";  
    document.getElementById("numero").value = "";
    document.getElementById("ciudad").value = "";
    document.getElementById("userId").value = "";
})